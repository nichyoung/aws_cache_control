#!/usr/bin/ruby

require 'aws-sdk'
require 'ostruct'
require 'optparse'

options = OpenStruct.new
options.bucket = nil
options.cache_control = "public, max-age=86400"
options.over_write = false
options.dry_run = false
options.profile = "default"
options.region = "ap-southeast-2"
options.verbose = false

OptionParser.new do |opts|
  opts.banner = "Usage: aws_cache_control.rb [options]"

  opts.on("-b", "--bucket BUCKET", "MANDATORY: AWS bucket to update objects in") do |b|
    options.bucket = b
  end

  opts.on("-c", "--cache-control CACHE CONTROL STRING", "Cache control string to set") do |c|
    options.cache_control = c
  end

  opts.on("-d", "--[no-]dry-run", "Don't actually write updated data to AWS") do |d|
    options.dry_run = d
  end

  opts.on("-o", "--[no-]over-write", "Update Cache-Control on all objects, even if already set") do |o|
    options.over_write = o
  end

  opts.on("-p", "--profile PROFILE", "Authentication profile to use, defaults to [default]") do |p|
    options.profile = p
  end

  opts.on("-r", "--region REGION", "AWS region to use, must be a valid AWS region") do |r|
    options.region = r
  end

  opts.on("-v", "--[no-]verbose", "Verbose") do |v|
    options.verbose = v
  end
end.parse!

throw OptionParser::MissingArgument if options.bucket.nil?

credentials = Aws::SharedCredentials.new(profile_name: options.profile)
Aws.config.update({region: options.region, credentials: credentials})

s3client = Aws::S3::Client.new
s3resource = Aws::S3::Resource.new(client: s3client)

bucket = s3resource.bucket(options.bucket)

objects = bucket.objects

objects.each do |os|
  begin
    if os.key[-1] == '/'
      puts "Skipping #{os.key} as it is a directory" if options.verbose
      next
    end
    o = bucket.object(os.key)
    if o.cache_control.nil? || options.over_write
      copy_opts = {:metadata_directive => "REPLACE", :copy_source => "#{os.bucket_name}/#{os.key}"}
      copy_opts[:cache_control] = options.cache_control
      copy_opts[:content_disposition] = o.content_disposition unless o.content_disposition.nil?
      copy_opts[:content_type] = o.content_type unless o.content_type.nil?
      copy_opts[:content_language] = o.content_language unless o.content_language.nil? 
      copy_opts[:expires] = o.expires unless o.expires.nil? 
      copy_opts[:content_encoding] = o.content_encoding unless o.content_encoding.nil? 
      copy_opts[:website_redirect_location] = o.website_redirect_location unless o.website_redirect_location.nil? 
      copy_opts[:metadata] = o.metadata unless o.metadata.nil? 
      o.copy_from(copy_opts) unless options.dry_run
      puts "Updating #{o.key}" if options.verbose
    else
      puts "Skipping #{o.key} already set to #{o.cache_control}" if options.verbose
    end
  rescue => e
    puts "Failed on #{os.key} #{e}"
    #punt the error and try the next object...
  end
end
